import 'datatables.net-bs5';
import 'datatables.net-buttons';
import 'datatables.net-bs5/css/dataTables.bootstrap5.css';
import moment from 'moment';
import {Tooltip} from 'bootstrap';
const routes = require('../public/js/fos_js_routes.json');
import Routing from '../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js';
import axios from "axios";
Routing.setRoutingData(routes);


let table = $('.table').DataTable({
    "language": {
        "url": "https://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Ukranian.json"
    },
    "dom": 'Bfrtip',
    "buttons": [
        {
            text: 'Додати користувача',
            className: "btn btn-primary",
            action: function ( e, dt, node, config ) {
                window.location.href = Routing.generate("admin_user_add");
            }
        }
    ],
    "responsive": true,
    "processing": true,
    "serverSide": true,
    "ajax": Routing.generate('admin_user_list_get_data'),
    "order": [3, "desc"],
    "initComplete": function () {
        let tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
        tooltipTriggerList.map(function (tooltipTriggerEl) {
            return new Tooltip(tooltipTriggerEl)
        });

        let table = this;
        document.querySelector("a.status-user").addEventListener("click", function (e) {
            e.preventDefault();
            axios.post(e.currentTarget.href, null, {
                responseType: "json"
            })
                .then(function (response) {
                    console.log(response);
                    modalAlert(response.data.msg, function () {
                        table.api().ajax.reload();
                    });
                })
                .catch(function (error) {
                    if(error.response) {
                        window.resetFormErrors();
                        window.generateFormErrors(error.response.data.errors);
                    }
                });
        });

        document.querySelector("a.del-user").addEventListener("click", function (e) {
            e.preventDefault();
            axios.post(e.currentTarget.href, null, {
                responseType: "json"
            })
                .then(function (response) {
                    console.log(response);
                    modalAlert(response.data.msg, function () {
                        table.api().ajax.reload();
                    });
                })
                .catch(function (error) {
                    if(error.response) {
                        window.resetFormErrors();
                        window.generateFormErrors(error.response.data.errors);
                    }
                });
        });
    },
    "columns": [
        {
            "data": "email",
            "searchable": true,
            "className": "desktop",
        },
        {
            "data": "roles",
            "searchable": false,
            "orderable": false,
            "render": function (data, type, row) {
                return data.join(', ');
            }
        },
        {
            "data": "lname",
            "searchable": true,
            "className": "desktop",
            "render": function (data, type, row) {
                let pib = row.lname + ' ' + row.fname;
                if(row.mname) {
                    pib += ' ' + row.mname;
                }

                return pib;
            }
        },
        {
            "data": "dateCreated",
            "searchable": false,
            "className": "text-center",
            "render": function (data, type, row) {
                if (data) {
                    return moment(data).format('DD/MM/YYYY HH:mm:ss');
                }
                return null;
            }
        },
        {
            "data": "lastLogin",
            "searchable": false,
            "className": "text-center",
            "render": function (data, type, row) {
                if (data) {
                    return moment(data).format('DD/MM/YYYY HH:mm:ss');
                }
                return null;
            }
        },
        {
            "data": "enabled",
            "searchable": false,
            "orderable": false,
            "render": function (data, type, row) {
                if (data) {
                    return '<span class="status label-success">Активний</span>'
                } else {
                    return '<span class="status label-danger">Неактивний</span>'
                }
            }
        },
        {
            "data": "id",
            "width": "10%",
            "searchable": false,
            "orderable": false,
            "render": function (data, type, row) {
                return '' +
                    '<div class="btn-group">' +
                    '<a href="' + Routing.generate('admin_user_edit', {"id": data}) + '" class="btn btn-outline-primary" data-bs-toggle="tooltip" data-bs-placement="top" title="Редагувати користувача">' +
                    '<i class="bi bi-pencil-square"></i>' +
                    '</a>' +
                    '<a href="' + Routing.generate('admin_user_status', {"id": data}) + '" class="btn btn-outline-primary status-user" data-bs-toggle="tooltip" data-bs-placement="top" title="' + ((row.enabled) ? 'Деактивувати' : 'Активувати') + '">' +
                    '<i class="bi bi-' + ((row.enabled) ? 'dash-square' : 'plus-square') + '"></i>' +
                    '</a>' +
                    '<a href="' + Routing.generate('admin_user_delete', {"id": data}) + '" class="btn btn-outline-primary del-user" data-bs-toggle="tooltip" data-bs-placement="top" title="Видалити користувача">' +
                    '<i class="bi bi-x-square"></i>' +
                    '</a>' +
                    '</div>';
            }
        }
    ]
});