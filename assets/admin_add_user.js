import Inputmask from "inputmask";
import 'slim-select/dist/slimselect.min.css';
import SlimSelect from 'slim-select';
import axios from "axios";

Inputmask().mask(document.querySelectorAll("input"));

new SlimSelect({
    select: 'select',
    searchPlaceholder: 'Пошук',
    searchText: 'Немає результатів',
    placeholder: 'Виберіть значення',
    searchingText: 'Пошук...',
});

const form = document.getElementById('form');
console.log(form);
form.addEventListener('submit', function (e) {
    event.preventDefault();
    axios.post(e.currentTarget.action, new FormData(e.currentTarget), {
        responseType: "json"
    })
        .then(function (response) {
            console.log(response);
            modalAlert(response.data.msg, function () {
                window.location.href = response.data.redirect_uri;
            });
        })
        .catch(function (error) {
            if(error.response) {
                window.resetFormErrors();
                window.generateFormErrors(error.response.data.errors);
            }
        });
});