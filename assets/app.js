/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.scss in this case)
import './styles/app.scss';

import { Modal } from 'bootstrap';

require('bootstrap');

window.resetFormErrors = function (form) {
    let el = form.elements;
    for (let i = 0; i < el.length; i++) {
        let elem = el[i];

        if (elem.classList.contains("is-invalid")) {
            elem.classList.remove("is-invalid");
        }
        console.log(elem);
        let invalidFeedback = elem.parentNode.querySelector("div.invalid-feedback");
        console.log(invalidFeedback);
        if (invalidFeedback) {
            invalidFeedback.remove();
        }
    }
}

window.generateFormErrors = function (errors, node) {
    for (var prop in errors) {
        console.log(parseInt(prop));
        if (parseInt(prop) >= 0) {
            window.modalAlert(errors[prop]);
        } else {
            let n;
            if (node === undefined) {
                n = "[" + prop + "]";

            } else {
                n = '[' + node + '][' + prop + ']';
            }
            if (!Array.isArray(errors[prop])) {
                generateFormErrors(errors[prop], prop)
            } else {
                let input = document.querySelector("form [name$='" + n + "']");
                if(!input){
                    input = document.querySelector("form [name$='" + n + "[]']");
                }

                input.classList.add("is-invalid");

                errors[prop].forEach((el) => {
                    let div = document.createElement("div");
                    div.className = "invalid-feedback d-block";
                    div.innerText = el;
                    input.after(div);
                });
            }
        }
    }
}

window.modalAlert = function(msg, callback) {
    let modalEl = document.getElementById('boostrap-modal');
    let modal = new Modal(modalEl);
    //let modalHeader = modalEl.querySelector('.modal-header');
    //let modalTitle = modalEl.querySelector('.modal-title');
    let modalBody = modalEl.querySelector('.modal-body p');

    /*if(!title) {
        modalHeader.hidden = true;
    } else {
        modalTitle.textContent = title;
    }*/

    modalBody.textContent = msg;
    modal.show();
    if(callback) {
        modalEl.addEventListener('hidden.bs.modal', function (event) {
            callback();
        }, {'once': true})
    }
}
