<?php

namespace App\Repository;

use App\Entity\User;
use App\Util\DataTableFormatter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(PasswordAuthenticatedUserInterface $user, string $newHashedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newHashedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    public function getUsersForDTG(Request $request)
    {
        $columns = [
            'id' => [
                'type' => Types::INTEGER
            ],
            'email' => [
                'type' => Types::STRING
            ],
            'lname' => [
                'type' => Types::STRING
            ],
            'fname' => [
                'type' => Types::STRING
            ],
            'mname' => [
                'type' => Types::STRING
            ],
            'dateCreated' => [
                'type' => Types::DATETIME_IMMUTABLE,
                'formats' => [
                    'datetime' => 'd/m/Y H:i:s',
                    'date' => 'd/m/Y'
                ]
            ],
            'lastLogin' => [
                'type' => Types::DATETIME_IMMUTABLE,
                'formats' => [
                    'datetime' => 'd/m/Y H:i:s',
                    'date' => 'd/m/Y'
                ]
            ],
            'roles' => [
                'type' => Types::SIMPLE_ARRAY
            ],
            'enabled' => [
                'type' => Types::BOOLEAN
            ]
        ];

        $data['draw'] = $request->get('draw');

        $qb = $this->createQueryBuilder('u');
        $qb
            ->select('count(u)')
            ->where('u.removed = false');
        $query = $qb->getQuery();
        $data['recordsTotal'] = $query->getSingleScalarResult();


        $qb = $this->createQueryBuilder('u');
        $qb->select('u.id as id, u.email, u.lname, u.fname, u.mname, u.dateCreated, u.lastLogin, u.roles, u.enabled')
            ->andWhere('u.removed = false');

        DataTableFormatter::prepare($request, $qb, $columns);

        $data['data'] = $qb->getQuery()->getArrayResult();

        $rfClass = new \ReflectionClass(User::class);
        $consts = $rfClass->getConstants();
        foreach ($data['data'] as &$value) {
            $rolesLabel = [];
            foreach ($value['roles'] as $val) {
                if (isset($consts[$val])) {
                    $rolesLabel[] = $consts[$val]['label'];
                }
            }
            $value['roles'] = $rolesLabel;
        }

        if (isset($options['search'])) {
            $qb->select('count(p.id)')
                ->setFirstResult(null)
                ->setMaxResults(null);
            $res = $qb->getQuery()->getSingleScalarResult();
            $data['recordsFiltered'] = $res;
        } else {
            $data['recordsFiltered'] = $data['recordsTotal'];
        }

        return $data;
    }
}