<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: 'users')]
#[ORM\HasLifecycleCallbacks]
#[UniqueEntity("email")]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    public final const ROLE_USER = [
        'label' => 'Користувач',
        'value' => 'ROLE_USER'
    ];

    public final const ROLE_STORAGE = [
        'label' => 'Користувач довідкової інформації',
        'value' => 'ROLE_STORAGE'
    ];

    public final const ROLE_OPERATOR = [
        'label' => 'Користувач довідкової інформації',
        'value' => 'ROLE_OPERATOR'
    ];
    public final const ROLE_SUPER_ADMIN = [
        'label' => 'Супер адмін',
        'value' => 'ROLE_SUPER_ADMIN'
    ];

    #[ORM\Id]
    #[ORM\GeneratedValue('IDENTITY')]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 180, unique: true)]
    private string $email;

    #[ORM\Column(type: 'json')]
    private array $roles = [];

    #[ORM\Column(type: 'string')]
    private string $password;

    #[ORM\Column(type: 'string', nullable: true)]
    private string $lname;

    #[ORM\Column(type: 'string', nullable: true)]
    private string $fname;

    #[ORM\Column(type: 'string', nullable: true)]
    private string $mname;

    #[ORM\Column(type: 'string', nullable: true)]
    private string $inn;

    #[ORM\Column(type: 'string', nullable: true)]
    private string $phone;

    #[ORM\Column(type: 'boolean')]
    private bool $enabled = false;

    #[ORM\Column(type: 'datetime')]
    private \DateTimeInterface $dateCreated;

    #[ORM\Column(type: 'datetime', nullable: true)]
    private \DateTimeInterface $lastLogin;

    #[ORM\Column(type: 'boolean')]
    private bool $removed = false;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUserIdentifier(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function hasRole($role)
    {
        return in_array(strtoupper($role), $this->getRoles(), true);
    }

    public function addRole(string $role)
    {
        $this->roles[] = $role;
    }

    /**
     * @see PasswordAuthenticatedUserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getLname(): ?string
    {
        return $this->lname;
    }

    public function setLname(?string $lname): User
    {
        $this->lname = $lname;
        return $this;
    }

    public function getFname(): ?string
    {
        return $this->fname;
    }

    public function setFname(?string $fname): User
    {
        $this->fname = $fname;
        return $this;
    }

    public function getMname(): ?string
    {
        return $this->mname;
    }

    public function setMname(?string $mname): User
    {
        $this->mname = $mname;
        return $this;
    }

    public function getInn(): ?string
    {
        return $this->inn;
    }

    public function setInn(?string $inn): User
    {
        $this->inn = $inn;
        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): User
    {
        $this->phone = $phone;
        return $this;
    }

    public function getEnabled(): bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): User
    {
        $this->enabled = $enabled;
        return $this;
    }

    public function getDateCreated(): ?\DateTimeInterface
    {
        return $this->dateCreated;
    }

    public function setDateCreated(?\DateTimeInterface $dateCreated): User
    {
        $this->dateCreated = $dateCreated;
        return $this;
    }

    public function getLastLogin(): ?\DateTimeInterface
    {
        return $this->lastLogin;
    }

    public function setLastLogin(?\DateTimeInterface $lastLogin): User
    {
        $this->lastLogin = $lastLogin;
        return $this;
    }

    public function getRemoved(): bool
    {
        return $this->removed;
    }

    public function setRemoved(bool $removed): User
    {
        $this->removed = $removed;
        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    #[ORM\PrePersist]
    public function prePersist()
    {
        $this->dateCreated = new \DateTime();
    }
}
