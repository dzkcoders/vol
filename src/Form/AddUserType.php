<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class AddUserType extends ProfileType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);
        $builder
            ->add('email', EmailType::class, [
                'label' => 'Email',
                'constraints' => [
                    new Assert\Email()
                ]
            ])
            ->add('roles', ChoiceType::class, [
                'label' => 'Роль',
                'help' => 'Використовуйте Ctrl + Click для вибору ролей',
                'choices' => $options['roles'],
                'multiple' => true,
                'constraints' => new Assert\NotBlank()
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);

        $resolver
            ->setDefault('roles', [])
            ->setAllowedTypes('roles', ['array']);
    }
}