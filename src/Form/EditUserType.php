<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class EditUserType extends ProfileType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('roles', ChoiceType::class, [
                'label' => 'Роль',
                'help' => 'Використовуйте Ctrl + Click для вибору ролей',
                'choices' => [
                    'Админістратор' => 'ROLE_ADMIN',
                    'Співробітник' => 'ROLE_HISTORY_MANAGER'
                ],
                'multiple' => true,
                'constraints' => new Assert\NotBlank()
            ])
            ->add('enabled', ChoiceType::class, [
                'label' => 'Активований',
                'choices' => [
                    'Так' => true,
                    'Ні' => false
                ],
                'multiple' => false,
                'expanded' => true,
                'constraints' => new Assert\NotBlank()
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        parent::configureOptions($resolver);

        $resolver
            ->setDefault('roles', [])
            ->setAllowedTypes('roles', ['array']);
    }
}