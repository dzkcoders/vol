<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class ProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class, [
                'label' => 'Email',
                'disabled' => true
            ])
            ->add('lname', TextType::class, [
                'label' => 'Прізвище',
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length(['max' => 255])
                ]
            ])
            ->add('fname', TextType::class, [
                'label' => 'І`мя',
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length(['max' => 255])
                ]
            ])
            ->add('mname', TextType::class, [
                'label' => 'По батькові',
                'required' => false,
                'constraints' => [
                    new Assert\Length(['max' => 255])
                ]
            ])
            ->add('inn', TextType::class, [
                'label' => 'ІПН',
                'attr' => [
                    'data-inputmask' => "'mask': '9', 'repeat': 10, 'greedy' : false"
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length(['max' => 10])
                ]
            ])
            ->add('phone', TextType::class, [
                'label' => 'Номер телефона',
                'attr' => [
                    'data-inputmask' => "'mask': '+38(099)999-99-99'"
                ],
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Length(['max' => 17])
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}