<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\AddUserType;
use App\Form\EditUserType;
use App\Util\FormHelper;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Route('admin/user', name: 'admin_user_')]
class UserController extends AbstractController
{
    #[Route('/add', name:'add', options: ['expose' => true])]
    #[Template]
    public function add(Request $request, UserInterface $currentUser, EntityManagerInterface $em, UserPasswordHasherInterface $passwordHasher, MailerInterface $mailer)
    {
        $user = (new User())
            ->setEnabled(true);

        $form = $this->createForm(AddUserType::class, $user, [
            'action' => $this->generateUrl('admin_user_add'),
            'roles' => $this->getPermittedRoles($currentUser->getRoles())
        ]);

        $form->handleRequest($request);
        if($form->isSubmitted()) {
            if($form->isValid()) {
                $passwordRaw = random_bytes(10);

                $user->setPassword($passwordHasher->hashPassword($user, $passwordRaw));

                $em->persist($user);
                $em->flush();

                $email = (new TemplatedEmail())
                    ->to($user->getEmail())
                    ->subject('Вам створено обліковий запис')
                    ->htmlTemplate('user/add_user_email.html.twig')
                    ->context([
                        'email_addr' => $user->getEmail(),
                    ]);

                $mailer->send($email);

                return $this->json([
                    'msg' => 'Користувач доданий',
                    'redirect_uri' => $this->generateUrl('admin_user_list')
                ]);
            }

            return $this->json([
                'errors' => FormHelper::getErrors($form)
            ], Response::HTTP_BAD_REQUEST);
        }

        return [
            'form' => $form->createView()
        ];
    }

    #[Route('/edit/{id}', name:'edit', options: ['expose' => true])]
    #[Template]
    public function edit(int $id, Request $request, UserInterface $currentUser, EntityManagerInterface $em)
    {
        $user = $em->getRepository(User::class)->findOneById($id);

        if(!$id) {
            $this->createNotFoundException();
        }

        $form = $this->createForm(EditUserType::class, $user, [
            'action' => $this->generateUrl('admin_user_edit', ['id' => $id]),
            'roles' => $this->getPermittedRoles($currentUser->getRoles())
        ]);

        $form->handleRequest($request);
        if($form->isSubmitted()) {
            if($form->isValid()) {
                $em->flush();

                return $this->json([
                    'msg' => 'Дані користувача відкореговані',
                    'redirect_uri' => $this->generateUrl('admin_user_list')
                ]);
            }

            return $this->json([
                'errors' => FormHelper::getErrors($form)
            ], Response::HTTP_BAD_REQUEST);
        }

        return [
            'form' => $form->createView()
        ];
    }

    #[Route('/list', name:'list')]
    #[Template]
    public function list()
    {
        return [
            'user_data_url' => $this->generateUrl('admin_user_list_get_data'),
        ];
    }

    #[Route('/get_data/list', name:'list_get_data', options: ['expose' => true])]
    public function listGetData(Request $request, EntityManagerInterface $em)
    {
        return $this->json(
            $em->getRepository(User::class)->getUsersForDTG($request, true)
        );
    }

    #[Route('/status/{id}', name:'status', options: ['expose' => true])]
    public function status($id, EntityManagerInterface $em)
    {
        $user = $em->getRepository(User::class)->findOneById($id);

        if(!$user) {
            throw $this->createNotFoundException();
        }
        if($user->isEnabled()) {
            $user->setEnabled(false);
            $msg = 'Користувач деактивований';
        } else {
            $user->setEnabled(true);
            $msg = 'Користувач активований';
        }

        $em->flush();

        return $this->json([
            'msg' => $msg
        ]);
    }

    #[Route('/delete/{id}', name:'delete', options: ['expose' => true])]
    public function delete($id, EntityManagerInterface $em)
    {
        $user = $em->getRepository(User::class)->findOneById($id);

        if(!$user) {
            throw $this->createNotFoundException();
        }
        if($user->isRemoved()) {
            throw new BadRequestException('User already deleted.');
        }

        $user->setRemoved(true);
        $em->flush();
        $msg = 'Користувач видалений';

        return $this->json([
            'msg' => $msg
        ]);
    }

    private function getPermittedRoles($roles) {

        return match (true) {
            in_array(USER::ROLE_SUPER_ADMIN['value'], $roles, true) => [
                User::ROLE_OPERATOR['label'] => User::ROLE_OPERATOR['value'],
                User::ROLE_USER['label'] => User::ROLE_USER['value']
            ],
            default => []
        };
    }
}