<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\ChangePasswordFormType;
use App\Form\ProfileType;
use App\Util\FormHelper;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

#[Route('back/profile')]
class ProfileController extends AbstractController
{
    #[Route('', name: 'profile')]
    #[Template]
    public function index(Request $request, UserInterface $user, EntityManagerInterface $em): Response|array
    {
        $form = $this->createForm(ProfileType::class, $user, [
            'action' => $this->generateUrl('profile')
        ]);

        $form->handleRequest($request);
        if($form->isSubmitted()){
            if($form->isValid()){
                $em->flush();

                return $this->json([
                    'msg' => 'Дані оновлені'],
                Response::HTTP_OK);
            }
        }

        return [
            'form' => $form->createView()
        ];
    }

    #[Route('/change_password', name: 'profile_change_password')]
    #[Template]
    public function changePassword(Request $request, UserInterface $user, EntityManagerInterface $em, UserPasswordHasherInterface $passwordHasher): Response|array
    {
        $form = $this->createForm(ChangePasswordFormType::class, $user, [
            'action' => $this->generateUrl('profile_change_password')
        ]);

        $form->handleRequest($request);
        if($form->isSubmitted()){
            if($form->isValid()){
                $hashedPassword = $passwordHasher->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                );
                $user->setPassword($hashedPassword);
                $em->flush();

                return $this->json([
                    'msg' => 'Пароль змінено'],
                    Response::HTTP_OK);
            }

            return $this->json([
                'errors' => FormHelper::getErrors($form)
            ], Response::HTTP_BAD_REQUEST);
        }

        return [
            'form' => $form->createView()
        ];
    }
}