<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

#[Route('/back', name: 'back_main_')]
class BackMainController extends AbstractController
{
    #[Route('', name: 'index')]
    #[Template]
    public function index()
    {
        return [];
    }
}