<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

#[Route('/', name: 'main_')]
class MainController extends AbstractController
{
    #[Route('', name: 'index')]
    #[Template]
    public function index()
    {
        return [];
    }
}