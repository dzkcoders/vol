<?php

namespace App\Util;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Query\Expr\Orx;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\Request;

class DataTableFormatter
{
    public const ORDER = [
        'asc' => 'ASC',
        'desc' => 'DESC'
    ];

    public static function prepare(Request $request, QueryBuilder $qb, $dbColumns)
    {
        $options = [];
        $tableColumns = $request->get('columns');
        foreach ($tableColumns as $value) {
            if ($value['searchable'] === 'true' && !empty($value['search']['value'])) {
                self::search($value['search']['value'], $value['data'], $dbColumns[$value['data']], $qb);
                if (!isset($options['search'])) {
                    $options['search'] = true;
                }
            }
        }
        if (!empty($order = $request->get('order'))) {
            self::order($order, $qb, $tableColumns);
        }

        if (!empty($limit = $request->get('length'))) {
            self::limit($limit, $qb);
        }

        if (!empty($start = $request->get('start'))) {
            self::offset($start, $qb);
        }
        $search = $request->get('search');

        if (!empty($search['value'])) {
            self::search($search, $tableColumns, $dbColumns, $qb);
            if (!isset($options['search'])) {
                $options['search'] = true;
            }
        }

        return $options;
    }

    private static function search($search, $tableColumns, $dbColumns, QueryBuilder $qb)
    {
        $orX = $qb->expr()->orX();

        if (is_array($tableColumns)) {
            foreach ($tableColumns as $index => $value) {
                if (isset($dbColumns[$value['data']])) {
                    $dbColumn = $dbColumns[$value['data']];
                    if ($value['searchable'] === 'true') {
                        if ($dbColumn['type'] === null) {
                            foreach ($dbColumns[$value['data']]['data'] as $k => $v) {
                                self::makeSearchCondition($search, $k, $dbColumn, $orX, $qb);
                            }
                        } else {
                            self::makeSearchCondition($search, $value['data'], $dbColumn, $orX, $qb);
                        }
                    }
                }
            }

            $qb->andWhere($orX);

            return null;
        }


        self::makeSearchCondition($search, $tableColumns, $dbColumns, $orX, $qb);
        $qb->andWhere($orX);

        return null;
    }

    private static function makeSearchCondition($search, $column, $dbColumn, Orx $orX, QueryBuilder $qb)
    {
        if ($dbColumn['type'] === Types::DATETIME_IMMUTABLE) {

            if (isset($dbColumn['formats']['datetime'])) {
                $datetime = \DateTime::createFromFormat($dbColumn['formats']['datetime'], $search['value']);

                if ($datetime instanceof \DateTime) {

                    $orX->add($qb->expr()->eq(self::getColumnAlias($column, $qb->getDQLPart('select'), $qb->getDQLPart('from')), ':' . $column));
                    $qb->setParameter($column, $datetime, Types::DATETIME_IMMUTABLE);
                } elseif (isset($dbColumn['formats']['date'])) {
                    $datetime = \DateTime::createFromFormat($dbColumn['formats']['date'], $search['value']);

                    if ($datetime instanceof \DateTime) {
                        $orX->add($qb->expr()->between(self::getColumnAlias($column, $qb->getDQLPart('select'), $qb->getDQLPart('from')), ':start_' . $column, ':end_' . $column));
                        $qb->setParameter('start_' . $column, $datetime->format('Y-m-d') . ' 00:00:00', Types::STRING);
                        $qb->setParameter('end_' . $column, $datetime->format('Y-m-d') . ' 23:59:59', Types::STRING);
                    }
                }
            }
        } else {
            if(is_array($search)) {
                if($search['regex'] && !isset($dbColumn['foreignKey'])) {
                    $orX->add($qb->expr()->like(self::getColumnAlias($column, $qb->getDQLPart('select'), $qb->getDQLPart('from')), ':' . $column));
                } /*else {
                    if(isset($dbColumn['foreignKey']) && is_numeric($search['value'])) {
                        $orX->add($qb->expr()->eq(self::getColumnAlias($column, $qb->getDQLPart('select'), $qb->getDQLPart('from')), ':' . $column));
                    }
                }*/

            } else {
                $orX->add($qb->expr()->eq(self::getColumnAlias($column, $qb->getDQLPart('select'), $qb->getDQLPart('from')), ':' . $column));
            }

            if(is_array($search)) {
                if($search['regex'] && !isset($dbColumn['foreignKey'])) {
                    $qb->setParameter($column, $search['value'] . '%');
                } /*else {
                    if(isset($dbColumn['foreignKey']) && is_numeric($search['value'])) {
                        $qb->setParameter($column, $search['value']);
                    }
                }*/
            } else {
                $qb->setParameter($column, $search);
            }
        }

        //$qb->orWhere($orX);
    }

    private static function order($order, QueryBuilder $qb, $columns)
    {
        foreach ($order as $value) {
            $column = array_slice($columns, $value['column'], 1)[0];
            if ($column['orderable']) {
                $qb->addOrderBy(self::getColumnAlias($column['data'], $qb->getDQLPart('select'), $qb->getDQLPart('from')), self::ORDER[$value['dir']]);
            }
        }
    }

    private static function limit($limit, QueryBuilder $qb)
    {
        if ($limit !== '-1') {
            $qb->setMaxResults($limit);
        }
    }

    private static function offset($offset, QueryBuilder $qb)
    {
        $qb->setFirstResult($offset);
    }

    /**
     * @param string $column
     * @param array $dglParts
     *
     * @return string
     */
    private static function getColumnAlias($column, array $select, array $from = null)
    {
        if ($from && count($from) === 1) {
            $fromAlias = $from[0]->getAlias();
        }

        foreach ($select as $dqlPart) {
            if (isset($fromAlias)) {
                if (in_array($fromAlias, explode(',', $dqlPart)) !== false) {
                    return $fromAlias . '.' . $column;
                }
            }

            if (preg_match(sprintf('/(?P<res1>[\w\d]+\.%s)|(?P<res2>[a-zA-z.]*)\s(as|AS)\s%s|(?P<res3>[a-z])\.{[\w\d,\s*]+%s/', $column, $column, $column), (string)$dqlPart, $matches)) {
                if (!empty($matches['res1'])) {
                    return $matches['res1'];
                }
                if (!empty($matches['res2'])) {
                    return $matches['res2'];
                }
                if (!empty($matches['res3'])) {
                    return sprintf('%s.%s', $matches['res3'], $column);
                }
            }
        }

        return $column;
    }
}