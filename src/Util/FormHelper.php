<?php

namespace App\Util;

use Symfony\Component\Form\FormInterface;

final class FormHelper
{
    /**
     * @param FormInterface $form
     *
     * @return array
     */
    public static function getErrors(FormInterface $form, $onlyRoot = false)
    {
        $errors = array();
        if (!$onlyRoot) {
            /** @var FormInterface $child */
            foreach ($form->getIterator() as $child) {
                if ($child->isSubmitted() && $child->count() > 0) {
                    $result = self::getErrors($child);
                    if (!empty($result)) {
                        $errors[$child->getName()] = $result;
                    }
                }

                foreach ($child->getErrors() as $key => $error) {
                    $errors[$child->getName()][] = $error->getMessage();
                }
            }
        }

        if ($form->isRoot() && $form->getErrors(false, false)) {
            foreach ($form->getErrors(false, false) as $key => $val) {
                $errors[$key] = $val->getMessage();
            }
        }

        return $errors;
    }
}